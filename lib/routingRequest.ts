/**
 * Class used to track active requests
 **/
class RoutingRequest {
  rpcId: string;
  nodeId: string;
  address: string;
  port: number;

  constructor(rpcId: string, prevNodeId: string, addr: string, port: number) {
    this.rpcId = rpcId;
    this.nodeId = prevNodeId;
    this.address = addr;
    this.port = port;
  }
}

export default RoutingRequest;
