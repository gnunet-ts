import { Base64Encode, Encrypt } from './encrypt';

describe('encrypt', () => {
  describe('Base64Encode', () => {
    it('should encode', () => {
      expect(new Base64Encode('test').encode()).toBe('dGVzdA==');
    });
    it('should decode', () => {
      expect(new Base64Encode('dGVzdA==').decode()).toBe('test');
    });
  });

  describe('Encrypt', () => {
    it('should encrypt', () => {
      expect(new Encrypt('foo').encode()).toBe(
        '3d7073ed0be00f798d1b1023a1eafa88'
      );
    });

    it('should return data unless encrypted flag is set', () => {
      expect(new Encrypt('foo').decode()).toBe('foo');
    });

    it('should decrypt if encrypted data is set', () => {
      const enc = new Encrypt('blub');
      enc.encrypted = '3d7073ed0be00f798d1b1023a1eafa88';
      expect(enc.decode()).toBe('foo');
    });
  });
});
