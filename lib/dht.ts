import { networkInterfaces } from 'os';
import { randomBytes } from 'crypto';
import AsyncTask from './asyncTask';
import { Base64Encode } from './encrypt';
import Globals from './globals';
import BaseKeyValidator from './keyValidator';
import MemCache, { isKeyValuePair, KeyInnerValuePair } from './memCache';
import PingRequest from './pingRequest';
import RoutingRequest from './routingRequest';
import RoutingTable from './routingTable';
import RPC from './rpc';
import UDPSocket from './sockets';
import { DistanceUtil } from './utils';
import RouteEntry, { isRouteEntry } from './routeEntry';

const deferred = require('deferred');

const logger = require('./logger').make('R5N/dht');

interface InterfaceEntry {
  name: string;
  address: string;
}

interface PeerDataMap {
  [key: string]: [KeyInnerValuePair];
}

interface CustomMessageHandlerOptions {
  messageType: string;
  onMessage: (rpc: RPC) => void;
  onResponse: () => void;
}

function makeRandomId() {
  const buf = randomBytes(Globals.KeyProps.DEFAULT_KEY_LENGTH);
  return buf.toString('hex');
}

function ensurefromAddressAndPort(
  rpc: RPC
): RPC & { fromAddress: string; fromPort: number } {
  if (!rpc.fromAddress) {
    throw new Error('fromAddress is required');
  }
  if (rpc.fromPort == null) {
    throw new Error('fromPort is required');
  }
  return rpc as any;
}

/**
 * The DHT instance. This class is responsible for booting the Node and general
 * Node operations.
 *
 * @param options required: IP, port. optional: key_validator_class
 **/
class DHT {
  key_validator_class: typeof BaseKeyValidator;
  address: string;
  port: number;
  routingTable: RoutingTable;
  cache: MemCache;
  socket: UDPSocket;
  id: string;
  pingRequests: PingRequest[];
  asyncTasks: AsyncTask[];
  routingRequests: RoutingRequest[];
  rpcToDefferedMap: { [key: string]: unknown };
  customMessageMap: { [key: string]: { onMessage: (rpc: RPC) => void } };

  constructor(options: any) {
    if (!options.IP || !options.port) {
      throw 'Address and port must be specified for the DHT.';
    }
    if (!options.key_validator_class) {
      this.key_validator_class = BaseKeyValidator;
    } else {
      this.key_validator_class = options.key_validator_class;
    }
    this.address = options.IP;
    this.port = options.port;
    this.routingTable = new RoutingTable(this);
    this.cache = new MemCache();
    this.socket = new UDPSocket(this.port);
    this.id = makeRandomId();
    // PingRequest queue awaiting resolution
    this.pingRequests = [];
    // List of AsyncTask's
    this.asyncTasks = [];
    // List of active RoutingRequest's
    // Needs to be a list and not a map because a given node can be
    // visited twice in a FIND_VALUE request so we're not guaranteed a
    // 1 to 1 matchup
    this.routingRequests = [];
    // Map of RPC ID -> Deferred object
    // Used to resolve a promise when a request is sent back to the initiator
    // TODO: Refactor this into a RPC Promise Manager class. We will need to track
    // any requests that haven't been fulfilled and resolved/reject them accordingly
    // with an AsyncTask
    this.rpcToDefferedMap = {};
    // Map which holds callback functions for custom message types
    this.customMessageMap = {};
  }

  start() {
    const d = deferred();
    this._init(d);
    return d.promise();
  }

  /**
   * Disconnects the DHT and ends network communication
   */
  stop() {
    this.socket.close();
    this._stopJobs();
  }

  private _stopJobs() {
    if (this.asyncTasks.length === 0) {
      return;
    }

    for (let i = 0; i < this.asyncTasks.length; i++) {
      const task = this.asyncTasks[i];
      task.stop();
    }
    this.asyncTasks = [];
  }

  private _init(def: any) {
    const self = this;

    this.socket
      .connect()
      .then(function (res: unknown) {
        logger.debug(
          'UDP socket for ID %s started on port %d',
          self.id,
          self.socket.port
        );
        self.socket.onMessage(
          DHT.prototype.onSocketMessage.bind(self),
          undefined
        );
        def.resolve({});
      })
      .done();

    // Add ping request clear job
    //    const clearPingTask = new AsyncTask('clear_ping_request_queue',
    //                                        DHT.prototype._checkPingRequestQueue.bind(this),
    //                                        Globals.AsyncTasks.CLEAR_PING_REQ_QUEUE);
    //    this.asyncTasks.push(clearPingTask);
    //    clearPingTask.start();

    // Add replication job
    const replicationTask = new AsyncTask(
      'replication_all',
      DHT.prototype._replicateAll.bind(this),
      Globals.AsyncTasks.REPLICATION_INTERVAL
    );
    this.asyncTasks.push(replicationTask);
    replicationTask.start();

    // Add data cleanup job
    const dataCleanupTask = new AsyncTask(
      'data_cleanup',
      DHT.prototype._clearOldData.bind(this),
      Globals.AsyncTasks.DATA_CLEANUP_INTERVAL
    );
    this.asyncTasks.push(dataCleanupTask);
    dataCleanupTask.start();

    // Add data cleanup job
    //    const staleNodesTask = new AsyncTask('stale_nodes_cleanup',
    //                                        DHT.prototype._checkForStaleNodes.bind(this),
    //                                        Globals.AsyncTasks.REMOVE_STALE_NODES_INTERVAL);
    //    this.asyncTasks.push(staleNodesTask);
    //    staleNodesTask.start();
  }

  /**
   * Clears any stale entries from the PingRequest queue and routing table
   **/
  private _checkPingRequestQueue() {
    const self = this;

    if (this.pingRequests.length === 0) {
      return;
    }

    const unResolved: PingRequest[] = [];
    for (let i = 0; i < this.pingRequests.length; i++) {
      const currReq = this.pingRequests[i];
      if (currReq.isExpired() || currReq.resolved) {
        this._removePingRequest(currReq.address, currReq.port);
        if (!currReq.resolved) {
          unResolved.push(currReq);
        }
      }
    }

    if (unResolved.length > 0) {
      logger.info('Clearing routing table %d: %j', this.port, unResolved);
      unResolved.forEach(function (pingReq) {
        self.routingTable.removeRoute(pingReq.address, pingReq.port);
      });
    }
  }

  /**
   * Checks if there is an existing PING request in the queue
   * @param addr
   * @param port
   * @return {boolean}
   * @private
   */
  private _pingRequestExists(addr: string, port: number) {
    if (this.pingRequests.length === 0) {
      return false;
    }

    let pingInQueue = false;
    this.pingRequests.forEach(function (pr) {
      if (pr.address === addr && pr.port === port) {
        pingInQueue = true;
        return false;
      }
      return true;
    });

    return pingInQueue;
  }

  /**
   * Removes a PING request from the queue
   * @param addr
   * @param port
   * @private
   */
  private _removePingRequest(addr: string, port: number) {
    if (this.pingRequests.length === 0) {
      return;
    }

    for (let i = 0; i < this.pingRequests.length; i++) {
      const currReq = this.pingRequests[i];
      if (currReq.address === addr && currReq.port === port) {
        return this.pingRequests.splice(i, 1);
      }
    }
    return null;
  }

  /**
   * Removes the given route request from the map
   **/
  private _clearRouteRequest(req: RoutingRequest) {
    for (let i = this.routingRequests.length - 1; i >= 0; i--) {
      const currReq = this.routingRequests[i];
      if (currReq.rpcId === req.rpcId) {
        return this.routingRequests.splice(i, 1);
      }
    }
    return null;
  }

  /**
   * Sends a PING request to each node in the routing table to check for stale nodes
   * If the node doesn't respond, it is removed from the routing table
   * @private
   */
  private _checkForStaleNodes() {
    const self = this;
    const allRoutes = this.routingTable.getAllRoutes();
    allRoutes.forEach(function (route) {
      self.sendPing(route.address, route.port);
    });
  }

  private _getNetworkInterfaces() {
    const ifaceList: InterfaceEntry[] = [];
    const ifaces = networkInterfaces();
    for (const dev in ifaces) {
      if (ifaces.hasOwnProperty(dev)) {
        let alias = 0;
        ifaces[dev]?.forEach(function (details) {
          if (details.family === 'IPv4') {
            ifaceList.push({
              name: dev + (alias ? ':' + alias : ''),
              address: details.address
            });
            ++alias;
          }
        });
      }
    }
  }

  private _encodeKey(key: string) {
    return new Base64Encode(key).encode();
  }

  private _decodeKey(key: string) {
    return new Base64Encode(key).decode();
  }

  get(key: string) {
    return this.cache.get(key);
  }

  put(key: string, value: unknown) {
    this.cache.put(key, value);
  }

  /**
   * Resolves a PING request
   **/
  private _resolvePing(addr: string, port: number) {
    this.pingRequests.forEach(function (req) {
      if (req.address === addr && req.port === port) {
        req.resolved = true;
        return false;
      }
      return true;
    });
  }

  /**
   * Bootstraps this DHT off of another. Works by sending FIND_NODE to
   * the given DHT.
   **/
  bootstrap(address: string, port: number) {
    return this.sendFindNode(address, port);
  }

  private _addRouteRequest(
    rpcId: string,
    nodeId: string,
    address: string,
    port: number
  ) {
    this.routingRequests.push(new RoutingRequest(rpcId, nodeId, address, port));
  }

  /**
   * Finds a routing request with the given node ID
   **/
  private _getRouteRequest(rpcId: string) {
    for (let i = this.routingRequests.length - 1; i >= 0; i--) {
      const currReq = this.routingRequests[i];
      if (currReq.rpcId === rpcId) {
        return currReq;
      }
    }
    return null;
  }

  /**
   * Adds a new deferred object to the RPC map
   **/
  private _createDeferredForRequest(rpcId: string) {
    if (this.rpcToDefferedMap[rpcId]) {
      return this.rpcToDefferedMap[rpcId];
    } else {
      const def = deferred();
      this.rpcToDefferedMap[rpcId] = def;
      return def;
    }
  }

  /**
   * Retrieves the deferred object for the given rpc ID
   **/
  private _getDeferredForRequest(rpcId: string) {
    return this.rpcToDefferedMap[rpcId];
  }

  /**
   *
   * @param {RPC} rpc
   * @returns {[RouteEntry}}
   * @private
   */
  private _getRandomUniqueRoute(rpc: RPC) {
    const randomRoutes = this.routingTable.getRandomRoutes(
      Globals.RecursiveRouting.MAX_RANDOM_NODES
    );
    let closest: RouteEntry | null = null;
    // We don't want to send back to the initiating node
    for (let i = 0; i < randomRoutes.length; i++) {
      //if (randomRoutes[i].port !== rpc.fromPort &&
      //    randomRoutes[i].address !== address)
      //{
      if (randomRoutes[i].port !== rpc.fromPort) {
        closest = randomRoutes[i];
        break;
      }
    }
    return closest;
  }

  private _replicate(key: string, value: unknown) {
    const routes = this.routingTable.getClosestPeers(key);

    // Here we send out a number of requests equal to the replication level
    for (
      let i = 0;
      i < routes.length && i < Globals.Network.getReplicationLevel();
      i++
    ) {
      const random = routes[i];
      const rpc = new RPC();
      rpc.type = 'STORE';
      rpc.address = random.address;
      rpc.port = random.port;
      // Set replication to true so that peers do not try to
      // re-play replication at point of storage
      rpc.set('replication', true);
      rpc.set('key', key);
      rpc.set('value', value);
      rpc.set('initiator_id', this.id);

      // Add to deferred list for replication only
      this._createDeferredForRequest(rpc.id);

      this.sendStore(rpc, random.address, random.port, 1);
    }
  }

  private _replicateBulk(peerDataMap: PeerDataMap) {
    for (const nodeId in peerDataMap) {
      if (peerDataMap.hasOwnProperty(nodeId)) {
        const route = this.routingTable.findByNodeId(nodeId);
        if (!route) {
          throw new Error('route not found');
        }
        const rpc = new RPC();
        rpc.type = 'STORE';
        rpc.address = route.address;
        rpc.port = route.port;
        // Set replication to true so that peers do not try to
        // re-play replication at point of storage
        rpc.set('replication', true);
        rpc.set('data', peerDataMap[nodeId]);
        rpc.set('initiator_id', this.id);

        // Add to deferred list for replication only
        this._createDeferredForRequest(rpc.id);

        this.sendStore(rpc, route.address, route.port, 1);
      }
    }
  }

  /**
   * Replicates all values to closest peers
   * @private
   */
  private _replicateAll() {
    const self = this;
    const allData = this.cache.all();
    if (allData.length === 0) {
      return;
    }
    logger.silly('Has data: %j', this.port, allData);

    const peerDataMap: PeerDataMap = {};
    const addToMap = (nodeId: string, data: KeyInnerValuePair) => {
      if (peerDataMap[nodeId]) {
        peerDataMap[nodeId].push(data);
      } else {
        peerDataMap[nodeId] = [data];
      }
    };

    allData.forEach(function (keyValue) {
      //_self._replicate(keyValue.key, keyValue.value);
      const routes = self.routingTable.getClosestPeers(keyValue.key);
      for (
        let i = 0;
        i < routes.length && i < Globals.Network.getReplicationLevel();
        i++
      ) {
        addToMap(routes[i].nodeId, keyValue);
      }
    });

    self._replicateBulk(peerDataMap);
  }

  /**
   * Clears out all old values from the DB
   * @private
   */
  private _clearOldData() {
    const allRows = this.cache.allAsValues();
    if (allRows.length === 0) {
      return;
    }

    for (let i = 0; i < allRows.length; i++) {
      const currVal = allRows[i].value;
      if (currVal.expired(Globals.ValueProps.TTL)) {
        logger.debug('Removing %j', allRows[i]);
        this.cache.removeValue(allRows[i].key);
      }
    }
  }

  /******** Message Senders **********/
  send(rpc: RPC) {
    if (!rpc.address || !rpc.port) {
      throw 'Invalid RPC request. Address and Port must be specified';
    }
    rpc.set('node_id', this.id);
    const data = rpc.serialize();
    return this.socket.send(data, rpc.address, rpc.port);
  }

  /**
   * Send ANNOUNCE message with closest peers
   **/
  sendAnnounce(rpc: RPC) {
    const closest = this.routingTable.getClosestPeers(rpc);

    // If there are no routes or the amount of returned routes is less
    // than the global constant K then add self
    if (closest.length === 0 || closest.length < Globals.Const.K) {
      closest.push(new RouteEntry(this.address, this.port, this.id));
    }

    rpc.type = 'ANNOUNCE';
    rpc.set('peers', closest);
    rpc.address = rpc.fromAddress;
    rpc.port = rpc.fromPort;
    this.send(rpc);
  }

  /**
   * In a PING call, this DHT is pinging another and expecting a return message of PONG
   * This can be used to check for stale nodes
   **/
  sendPing(addr: string, port: number) {
    const rpc = new RPC();
    rpc.type = 'PING';
    rpc.address = addr;
    rpc.port = port;
    this.send(rpc);

    if (!this._pingRequestExists(addr, port)) {
      this.pingRequests.push(new PingRequest(addr, port));
    }
  }

  sendPong(addr: string, port: number) {
    const rpc = new RPC();
    rpc.type = 'PONG';
    rpc.address = addr;
    rpc.port = port;
    this.send(rpc);
  }

  /**
   * Sends a FIND_NODE message to a peer.
   **/
  sendFindNode(addr: string, port: number) {
    const rpc = new RPC();
    rpc.type = 'FIND_NODE';
    rpc.address = addr;
    rpc.port = port;

    const def = this._createDeferredForRequest(rpc.id);
    this.send(rpc);
    return def.promise();
  }

  /**
   * Initiates a STORE request
   **/
  store(key: string, value: unknown) {
    let def;
    const keyValidator = new this.key_validator_class(key);
    if (!keyValidator.isValid()) {
      // No need to forward request
      def = deferred();
      def.reject(new Error('Invalid key: ' + key));
      return def.promise();
    }

    if (this.cache.containsKey(key)) {
      // No need to forward request
      def = deferred();
      def.resolve({ key: key, value: this.cache.get(key) });
      return def.promise();
    }

    // Get a random route and start the hop counter at 1
    const randomRoute = this.routingTable.getRandomRoutes(
      Globals.RecursiveRouting.MAX_RANDOM_NODES
    )[0];
    const rpc = new RPC();
    rpc.type = 'STORE';
    rpc.address = randomRoute.address;
    rpc.port = randomRoute.port;
    rpc.set('key', key);
    rpc.set('value', value);
    rpc.set('initiator_id', this.id);
    // Not replication
    rpc.set('replication', false);

    // Add to deferred map
    def = this._createDeferredForRequest(rpc.id);
    this.sendStore(rpc, randomRoute.address, randomRoute.port, 1);
    return def.promise();
  }

  /**
   * Forwards a STORE request
   **/
  sendStore(rpc: RPC, addr: string, port: number, hopCount: number) {
    rpc.address = addr;
    rpc.port = port;
    rpc.set('hop_count', hopCount);
    this.send(rpc);
  }

  /**
   * Forwards the request back to the prev node
   **/
  sendForward(rpc: RPC) {
    const routeReq = this._getRouteRequest(rpc.id);
    if (!routeReq) {
      logger.warn('No routing information for RPC ID %s', rpc.id);
      return;
    }

    if (rpc.get('original_type') == null) {
      const origType = rpc.type;
      rpc.set('original_type', origType);
    }

    rpc.type = 'FORWARD';
    rpc.address = routeReq.address;
    rpc.port = routeReq.port;

    // Remove the routing request
    this._clearRouteRequest(routeReq);
    this.send(rpc);
  }

  /**
   * Initiates a FIND_VALUE operation
   **/
  findValue(key: string) {
    const randomRoute = this.routingTable.getRandomRoutes(
      Globals.RecursiveRouting.MAX_RANDOM_NODES
    )[0];
    const rpc = new RPC();
    rpc.type = 'FIND_VALUE';
    rpc.set('key', key);
    rpc.address = randomRoute.address;
    rpc.port = randomRoute.port;
    rpc.set('initiator_id', this.id);
    // Number of times this RPC has been sent around
    rpc.set('attempts', 0);

    // Add to deferred map
    const def = this._createDeferredForRequest(rpc.id);
    this.sendFindValue(rpc, randomRoute.address, randomRoute.port, 1);
    return def.promise();
  }

  /**
   * Retries a FIND_VALUE operation
   **/
  private _retryFindValue(rpc: RPC) {
    const randomRoute = this.routingTable.getRandomRoutes(
      Globals.RecursiveRouting.MAX_RANDOM_NODES
    )[0];
    const attempts = rpc.getNumberOrThrow(
      'attempts',
      'attempts must be a number'
    );
    rpc.set('attempts', attempts + 1);
    rpc.type = 'FIND_VALUE';
    rpc.address = randomRoute.address;
    rpc.port = randomRoute.port;
    this.sendFindValue(rpc, randomRoute.address, randomRoute.port, 1);
  }

  /**
   * Sends a FIND_VALUE message
   **/
  sendFindValue(rpc: RPC, addr: string, port: number, hopCount: number) {
    rpc.address = addr;
    rpc.port = port;
    rpc.set('hop_count', hopCount);
    this.send(rpc);
  }

  /******** Message Handlers **********/
  onSocketMessage(rpc: RPC) {
    this._handleMessage(rpc);
  }

  private _handleMessage(rpc: RPC) {
    //logger.debug('Received %s from %d', rpc.type, rpc.fromPort);
    switch (rpc.type) {
      case 'PING':
        this.onPingMessage(rpc);
        break;
      case 'PONG':
        this.onPongMessage(rpc);
        break;
      case 'FIND_NODE':
        this.onFindNodeMessage(rpc);
        break;
      case 'ANNOUNCE':
        this.onAnnounceMessage(rpc);
        break;
      case 'STORE':
        this.onStoreMessage(rpc);
        break;
      case 'FIND_VALUE':
        this.onFindValueMessage(rpc);
        break;
      case 'FORWARD':
        // TODO: Figure out if this is the correct way to send responses back
        // to the initiating server
        this.onForwardMessage(rpc);
        break;
      default:
        this._handleCustomMessage(rpc);
    }
  }

  /**
   * A response from FIND_NODE rpc. Should contain node_id and peers for data.
   **/
  onAnnounceMessage(rpc: RPC) {
    const self = this;
    const _peers = rpc.get('peers');

    const peers = Array.isArray(_peers) ? (_peers as unknown[]) : null;
    if (!peers) {
      throw new Error('peers is not an array');
    }

    peers.forEach(function (peer) {
      if (!isRouteEntry(peer)) {
        throw new Error('peer is not a route entry');
      }
      self.routingTable.addRoute(peer.address, peer.port, peer.nodeId);
      // Send ping to add this node to peer node routing tables
      self.sendPing(peer.address, peer.port);
    });

    logger.silly('Has %d routes', this.routingTable.getAllRoutes().length);
    const def: any = this._getDeferredForRequest(rpc.id);
    if (def) {
      def.resolve({
        number_of_routes: this.routingTable.getAllRoutes().length
      });
    }
  }

  onPingMessage(_rpc: RPC) {
    const rpc = ensurefromAddressAndPort(_rpc);
    // Add the node to the routing table
    this.routingTable.addRoute(
      rpc.fromAddress,
      rpc.fromPort,
      rpc.getStringOrThrow('node_id', 'node_id must be a string')
    );
    // Respond with pong
    this.sendPong(rpc.fromAddress, rpc.fromPort);
  }

  onPongMessage(_rpc: RPC) {
    const rpc = ensurefromAddressAndPort(_rpc);
    // check pending ping requests and resolve it if it exists
    // and isn't expired
    this._resolvePing(rpc.fromAddress, rpc.fromPort);
  }

  /**
   * For a FIND_NODE message, we get the closest peers to the given node ID
   * up to a maximum K and return them
   **/
  onFindNodeMessage(rpc: RPC) {
    this.sendAnnounce(rpc);
  }

  /**
   * Handles a STORE message.
   * STEPS:
   *   1) Get the nodes closest to the given key in the routing table
   *   2) If this node's ID is closer than all peers, store the value locally
   *   3) Else, Forward the STORE request to the closest peer
   **/
  onStoreMessage(_rpc: RPC) {
    const rpc = ensurefromAddressAndPort(_rpc);
    // Add a new routing link
    // NOTE: THIS HAS TO BE AT THE TOP FOR FORWARDING REQUESTS
    this._addRouteRequest(
      rpc.id,
      rpc.getStringOrThrow('node_id'),
      rpc.fromAddress,
      rpc.fromPort
    );

    let keyValidator;
    if (rpc.get('data')) {
      // This is a bulk replication operation
      // Just load all the data and return
      const _replData = rpc.get('data');

      const replData = Array.isArray(_replData)
        ? (_replData as unknown[])
        : null;
      if (!replData) {
        throw new Error('data is not an array');
      }

      rpc.set('stored_at', this.id);
      for (let i = 0; i < replData.length; i++) {
        const kv = replData[i];
        if (!isKeyValuePair(kv)) {
          throw new Error('expected a key value object');
        }
        keyValidator = new this.key_validator_class(kv.key);
        if (!keyValidator.isValid()) {
          logger.warn('Invalid key: %s', kv.key);
          // Send reply
          this.sendForward(rpc);
          return;
        }
        this.cache.put(kv.key, kv.value);
      }

      this.sendForward(rpc);
      return;
    }

    const token = rpc.getStringOrThrow('key');
    keyValidator = new this.key_validator_class(token);
    if (!keyValidator.isValid()) {
      logger.warn('Invalid key: %s', token);
      rpc.set('stored_at', this.id);
      // Send reply
      this.sendForward(rpc);
      return;
    }

    logger.silly('At node for store: %s', this.id);
    const key = keyValidator.parse().key;
    let hops = rpc.getNumberOrThrow('hop_count', 'hop_count must be a number');

    let closestRoute: RouteEntry | null = null;
    let randomRoute = false;
    if (hops > Globals.RecursiveRouting.MAX_RANDOM_NODES) {
      closestRoute = this.routingTable.getClosestPeers(rpc)[0];
    } else {
      closestRoute = this._getRandomUniqueRoute(rpc);
      randomRoute = true;
    }

    // If the closest route is NULL, that means that there is probably
    // only 1 entry in the routing table. If so just store the value here
    if (!closestRoute) {
      closestRoute = new RouteEntry(this.address, this.port, this.id);
      randomRoute = false;
    }

    hops++;

    const address = closestRoute.address;
    const port = closestRoute.port;
    const isReplication = rpc.get('replication');

    if (!randomRoute) {
      const myDistance = DistanceUtil.calcDistance(key, this.id);
      const closestDistance = DistanceUtil.calcDistance(
        key,
        closestRoute.nodeId
      );

      if (myDistance <= closestDistance) {
        // Store here
        logger.info('Stored a KV. %s: %s', rpc.get('key'), rpc.get('value'));
        this.cache.put(token, rpc.get('value'));
        rpc.set('stored_at', this.id);
        // Send reply
        this.sendForward(rpc);

        // Replication step
        if (!isReplication) {
          this._replicate(
            rpc.getStringOrThrow('key', 'key must be a string'),
            rpc.get('value')
          );
        }
      } else {
        // Send store request to next node
        this.sendStore(rpc, address, port, hops);
      }
    } else {
      this.sendStore(rpc, address, port, hops);
    }
  }

  /**
   * Handles FIND_VALUE message. Steps are the same as STORE but instead
   * we are retrieving.
   * TODO: Refactor both the onFindValue and onStore methods into a more abstract
   * structure
   **/
  onFindValueMessage(_rpc: RPC) {
    const rpc = ensurefromAddressAndPort(_rpc);
    // Add a new routing link
    // NOTE: THIS HAS TO BE AT THE TOP FOR FORWARDING REQUESTS
    this._addRouteRequest(
      rpc.id,
      rpc.getStringOrThrow('node_id', 'node_id must be a string'),
      rpc.fromAddress,
      rpc.fromPort
    );

    const token = rpc.getStringOrThrow('key');
    const keyValidator = new this.key_validator_class(token);
    if (!keyValidator.isValid()) {
      logger.warn('Invalid key: %s', token);
      rpc.set('found', false);
      rpc.set('retrieved_at', this.id);
      // Send reply
      this.sendForward(rpc);
      return;
    }

    // Check if the value is stored locally
    // If it is, return it
    // If not, calculate
    const key = keyValidator.parse().key;
    let hops = rpc.getNumberOrThrow('hop_count', 'hop_count must be a number');

    // Check for existence of data locally then
    // forward if not found
    const value = this.cache.get(token);
    if (value) {
      rpc.set('value', value);
      rpc.set('found', true);
      rpc.set('found_at', this.id);
      // Send reply
      this.sendForward(rpc);
      return;
    }

    let closestRoute: RouteEntry | null = null;
    let randomRoute = false;
    if (hops > Globals.RecursiveRouting.MAX_RANDOM_NODES) {
      closestRoute = this.routingTable.getClosestPeers(rpc)[0];
    } else {
      closestRoute = this._getRandomUniqueRoute(rpc);
      randomRoute = true;
    }

    // If the closest route is NULL, that means that there is probably
    // only 1 entry in the routing table. If so just try to find the value here
    if (!closestRoute) {
      closestRoute = new RouteEntry(this.address, this.port, this.id);
      randomRoute = false;
    }

    hops++;

    const address = closestRoute.address;
    const port = closestRoute.port;

    logger.info(
      'RPC ID: %s\nInit ID: %s\nLast node ID: %s\n',
      rpc.id,
      rpc.get('initiator_id'),
      rpc.get('node_id')
    );
    if (!randomRoute) {
      const myDistance = DistanceUtil.calcDistance(key, this.id);
      const closestDistance = DistanceUtil.calcDistance(
        key,
        closestRoute.nodeId
      );
      logger.silly(
        'onStoreMessage. myDistance = %d, closestDist = %d',
        myDistance,
        closestDistance
      );

      if (myDistance <= closestDistance) {
        // We have reached the closest node and we don't have the key
        // return failure
        logger.warn('At closest node with no value port: %d', this.port);
        rpc.set('found', false);
        rpc.set('retrieved_at', this.id);
        // Send reply
        this.sendForward(rpc);
      } else {
        // Send store request to next node
        this.sendFindValue(rpc, address, port, hops);
      }
    } else {
      logger.silly('Random hop #%d to peer %s:%d', hops - 1, address, port);
      this.sendFindValue(rpc, address, port, hops);
    }
  }

  /**
   * Handles a FORWARD message.
   * Check to see if the initiating node ID is this DHT's ID
   * If it is, the request has reached the initiator
   * If not, remove the entry from the routing request list and
   * forward the request to the next node in the chain
   **/
  // TODO Refactor: too many nested ifs and elses means the function can be split.
  onForwardMessage(rpc: RPC) {
    logger.silly('Received forward message.');
    if (rpc.get('initiator_id') === this.id) {
      logger.silly('Reached initiator.');
      const def: any = this._getDeferredForRequest(rpc.id);

      if (rpc.get('is_custom_type')) {
        this._handleCustomForwardMessage(rpc);
        return;
      }
      // This callback will handle both FIND_VALUE and STORE RPC's
      if (rpc.get('original_type') === 'FIND_VALUE') {
        // Only resolve the promise if a value was found
        if (rpc.get('found') === true) {
          def.resolve({
            rpc: rpc
          });
        } else {
          if (
            rpc.getNumberOrThrow('attempts', 'attempts must be a number') >
            Globals.RecursiveRouting.MAX_FIND_VALUE_ATTEMPTS
          ) {
            def.reject(new Error('No value found for key: ' + rpc.get('key')));
          } else {
            // Retry request
            this._retryFindValue(rpc);
          }
        }
      } else {
        def.resolve({ rpc: rpc });
      }
    } else {
      this.sendForward(rpc);
    }
  }

  /**
   * Forwards this message to the correct custom handler
   * @param {RPC} rpc
   * @private
   */
  private _handleCustomMessage(rpc: RPC) {
    const cbMap = this._getCallbacksForRPC(rpc.type);
    if (!cbMap) {
      logger.warn('Unsupported message type: %s', rpc.type);
      return;
    }

    cbMap.onMessage.call(this, rpc);
  }

  /**
   * Returns the callbacks for the specified RPC type
   * @param {string} type
   * @return {object} - Object containing callbacks for the RPC type or NULL
   * @private
   */
  private _getCallbacksForRPC(type: string | null) {
    if (!type) {
      return null;
    }
    const cbMap = this.customMessageMap[type];
    if (!cbMap) {
      return null;
    }
    return cbMap;
  }

  /**
   * Handler function for custom message forwarding
   * @param rpc
   * @private
   */
  private _handleCustomForwardMessage(rpc: RPC) {
    const cbMap = this._getCallbacksForRPC(
      rpc.getStringOrThrow('original_type', 'original_type must be a string')
    );
    const def: any = this._getDeferredForRequest(rpc.id);

    if (!cbMap) {
      logger.warn('Unsupported message type: %s', rpc.get('original_type'));
      def.reject(
        new Error('Unsupported message type: ' + rpc.get('original_type'))
      );
      return;
    }

    def.resolve({ rpc: rpc });
  }

  /**
   * Add a custom message type to the DHT
   *
   * @param {object} options - Object containing the following fields:
   * @param options.messageType - A unique message type string
   * @param options.onMessage - Callback called when a message is received with
   * the specified message type.
   * @param options.onResponse - Callback called when the initiating node receives
   * the response for a custom message
   */
  addCustomMessageHandler(options: CustomMessageHandlerOptions) {
    const self = this;
    if (!options.messageType || !options.onMessage) {
      throw new Error(
        'The following fields are required for custom message ' +
          'handlers: messageType, onMessage'
      );
    }
    this.customMessageMap[options.messageType] = {
      onMessage: function (_rpc: RPC) {
        const rpc = ensurefromAddressAndPort(_rpc);
        self._addRouteRequest(
          rpc.id,
          rpc.getStringOrThrow('node_id', 'node_id must be a string'),
          rpc.fromAddress,
          rpc.fromPort
        );
        options.onMessage.call(self, rpc);
      }
    };
  }

  /**
   * Sends a custom message. Throws exception if message handlers have not
   * been defined.
   *
   * @param {string} type
   * @param {RPC} rpc
   */
  sendCustomRPC(type: string, rpc: RPC) {
    if (!type || !this._getCallbacksForRPC(type)) {
      throw 'No message handlers defined for message type: "' + type + '"';
    }
    if (!rpc.address || !rpc.port) {
      throw 'Remote server address and port are required for RPC transport.';
    }

    if (!rpc.get('initiator_id')) {
      rpc.set('initiator_id', this.id);
    }

    rpc.type = type;
    rpc.set('is_custom_type', true);
    this.send(rpc);

    const def = this._createDeferredForRequest(rpc.id);
    return def.promise();
  }

  /**
   * Signifies that the transmission and processing of the RPC is complete.
   * At this point, responses are sent back to the initiating node.
   * @param {RPC} rpc
   */
  endRPC(rpc: RPC) {
    this.sendForward(rpc);
  }

  static RPC = RPC;
  static BaseKeyValidator = BaseKeyValidator;
}

export default DHT;
