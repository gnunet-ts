import RPC from './rpc';

describe('rpc', () => {
  describe('constructor', () => {
    it('initializes to empty object as default data', () => {
      const rpc = new RPC();
      expect(rpc.data).toEqual({});
    });

    it('initializes correctly', () => {
      const rpc = new RPC({ some: 'thing' });
      expect(rpc.data.some).toBe('thing');
      expect(rpc.id.length).toBe(64);
      expect(rpc.type).toBeNull();
      expect(rpc.address).toBeNull();
      expect(rpc.port).toBeNull();
      expect(rpc.fromAddress).toBeNull();
      expect(rpc.fromPort).toBeNull();
      expect(rpc.fromFamily).toBeNull();
      expect(rpc.packetSize).toBeNull();
    });

    it('initializes by copying data fields', () => {
      const rpc = new RPC({
        some: 'thing',
        id: 'myId',
        type: 'myType',
        address: 'myAddress',
        port: 8080,
        fromAddress: 'ignored',
        fromPort: 'ignored',
        fromFamily: 'ignored',
        packetSize: 'ignored'
      });
      expect(rpc.data.some).toBe('thing');
      expect(rpc.id).toBe('myId');
      expect(rpc.type).toBe('myType');
      expect(rpc.address).toBe('myAddress');
      expect(rpc.port).toBe(8080);
      // not copied fields
      expect(rpc.fromAddress).toBeNull();
      expect(rpc.fromPort).toBeNull();
      expect(rpc.fromFamily).toBeNull();
      expect(rpc.packetSize).toBeNull();
    });
  });

  it('setRemoteData sets the remote fields only', () => {
    const rpc = new RPC({ some: 'thing' });
    rpc.setRemoteData({
      some: 'ignored',
      id: 'ignored',
      type: 'ignored',
      address: 'address',
      port: 8080,
      family: 'family',
      size: 1234
    });
    expect(rpc.id).not.toBe('ignored');
    expect(rpc.type).toBeNull();
    expect(rpc.address).toBeNull();
    expect(rpc.port).toBeNull();
    // copied fields
    expect(rpc.fromAddress).toBe('address');
    expect(rpc.fromPort).toBe(8080);
    expect(rpc.fromFamily).toBe('family');
    expect(rpc.packetSize).toBe(1234);
  });

  it('serialize has a side-effect on data', () => {
    const rpc = new RPC({ my: 'stuff' });
    rpc.setRemoteData({
      address: 'myAddress',
      port: 8080,
      family: 'foo',
      size: 42
    });
    expect(rpc.data).toEqual({ my: 'stuff' });
    rpc.serialize();
    expect(rpc.data.my).toBe('stuff');
    expect(rpc.data.id?.length).toBe(64);
    expect(rpc.data.address).toBeNull();
  });

  it('serializes correctly', () => {
    const rpc = new RPC();

    const str = rpc.serialize();
    const parsed = JSON.parse(str);
    parsed.id = null; // remove

    expect(parsed).toEqual({
      address: null,
      id: null,
      port: null,
      type: null
    });
  });

  describe('set updates the data object', () => {
    const rpc = new RPC();
    rpc.set('foo', 42);
    expect(JSON.parse(rpc.serialize()).foo).toBe(42);
  });

  describe('get', () => {
    const rpc = new RPC();
    rpc.set('foo', 42);
    expect(rpc.get('foo')).toBe(42);
  });
});
