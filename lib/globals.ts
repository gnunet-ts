/**
 * The Globals object contains all system wide globals vars
 * NOTES:
 *    1) All time is expressed in milliseconds (ms)
 *    2) DO NOT change any config values if you don't know what they affect.
 *    Always consult the R5N repo README.
 **/
const Globals = Object.freeze({
  Const: {
    // Global constant for number of peers to return in FIND_NODE op
    K: 5,
  },

  Network: {
    DHT_PUBLIC_ADDRESS: null,
    // DHT start port
    DHT_INIT_PORT: 8000,
    // Maximum time to wait for a PING response (ms)
    PING_MAX_TIME: 20000, // 20 seconds

    // Vars used to calculate replication level
    // Estimated number of connected peers on the network
    ESTIMATED_PEER_COUNT: 150,
    // Estimate number of connections to other peers that a given peer will have
    ESTIMATED_CONN_PER_PEER: 5,
    // Calculated replication level based off the R5N spec
    // Replication level is the square root of the Markov mixing time of a graph
    // which is N / (C + 1) where N is the estimated number of peers on the network
    // and C is the estimated number of connections per peer
    getReplicationLevel: function () {
      return Math.ceil(
        Math.sqrt(
          this.ESTIMATED_PEER_COUNT / (this.ESTIMATED_CONN_PER_PEER + 1)
        )
      );
    },
  },

  Routing: {
    // Max size for a routing table
    ROUTE_TABLE_MAX_SIZE: 20,
  },

  // Properties for keys
  KeyProps: {
    // Default key size for Node Id's, KV pairs etc
    DEFAULT_KEY_LENGTH: 64,
    // Delimiter for key namespaces
    KEY_DELIMITER: ":",
  },

  // Properties for values
  ValueProps: {
    // TTL for values
    // After this point values will be garbage collected
    TTL: 86400000, // 1 Day
  },

  RecursiveRouting: {
    // Max number of random nodes to start with
    // TODO: There is a bug with the addRoute() method in RoutingTable
    // Which causes duplicates to be added to the routing table which
    // causes the getAllRoutes() method to return duplicate entries at random times
    // Setting this to 3 to get around that for the moment
    MAX_RANDOM_NODES: 3,
    // Maximum amount of time to recurse
    MAX_STACK_SIZE: 20,
    // Number of times to rety a FIND_VALUE operation
    MAX_FIND_VALUE_ATTEMPTS: 3,
  },

  AsyncTasks: {
    // Interval to send PING requests to check for stale nodes
    REMOVE_STALE_NODES_INTERVAL: 300000, // 5 minutes
    // Interval for clearing the ping request queue
    // This also clears stale nodes from the routing table
    CLEAR_PING_REQ_QUEUE: 20000, // 60 seconds
    // Replication job interval. Recommended setting: 2-5 minutes.
    // This will replicate all values to closest peer nodes
    REPLICATION_INTERVAL: 120000, // 2 minutes
    // Interval for clearing old data
    DATA_CLEANUP_INTERVAL: 300000, // 5 minutes
  },
});

export default Globals;
