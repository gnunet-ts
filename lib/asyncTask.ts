export const INITIAL_DELAY = 20000;

/**
 * AsyncTask class used to perform tasks at a specified interval
 **/
class AsyncTask {
  name: string;
  fn: Function;
  interval?: number;
  intervalId: ReturnType<typeof setInterval> | -1;
  stopped: boolean;

  constructor(name: string, fn: Function, interval?: number) {
    this.name = name;
    this.fn = fn;
    this.interval = interval;
    this.intervalId = -1;
    // Value set when stop() is called
    // This prevents a task from starting if stop is called before
    // the interval is set
    this.stopped = false;
  }

  start() {
    const self = this;
    setTimeout(function () {
      if (self.stopped) {
        return;
      }
      self.intervalId = setInterval(function () {
        //console.log('Running job: %s', _self.name);
        self.fn();
      }, self.interval);
    }, INITIAL_DELAY);
  }

  stop() {
    this.stopped = true;
    clearInterval(this.intervalId);
  }
}

export default AsyncTask;
