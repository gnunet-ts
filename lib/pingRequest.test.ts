import PingRequest from './pingRequest';

describe('ping_request', () => {
  it('should store fields correctly', () => {
    const req = new PingRequest('1.2.3.4', 8080);
    expect(req.address).toBe('1.2.3.4');
    expect(req.port).toBe(8080);
    expect(req.resolved).toBe(false);
    expect(typeof req.timestamp).toBe('number');
  });

  it('should not be expired right after creation', () => {
    const req = new PingRequest('1.2.3.4', 8080);
    expect(req.isExpired()).toBe(false);
  });

  it('should not be expired after 20 seconds', () => {
    const req = new PingRequest('1.2.3.4', 8080);
    expect(req.isExpired()).toBe(false);
    req.timestamp = req.timestamp - 30000; // FIXME: modifies timestamp directly
    expect(req.isExpired()).toBe(true);
  });

  it('should not be resolved unless set', () => {
    const req = new PingRequest('1.2.3.4', 8080);
    expect(req.isResolved()).toBe(false);
    req.setResolved(true);
    expect(req.isResolved()).toBe(true);
  });
});
