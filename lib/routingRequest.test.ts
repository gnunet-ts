import RoutingRequest from './routingRequest';

describe('routing_request', () => {
  it('assigns the correct fields', () => {
    const entry = new RoutingRequest('abc', 'prevId', '1.2.3.4', 8080);
    expect(entry.rpcId).toBe('abc');
    expect(entry.nodeId).toBe('prevId');
    expect(entry.address).toBe('1.2.3.4');
    expect(entry.port).toBe(8080);
  });
});
