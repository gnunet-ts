/**
 * Entry stored in a routing table. Contains information used to lookup
 * adjacent nodes.
 **/
class RouteEntry {
  address: string;
  port: number;
  nodeId: string;

  constructor(address: string, port: number, nodeId: string) {
    this.address = address;
    this.port = port;
    this.nodeId = nodeId;
  }

  /**
   * Will return true only if PORT and ADDRESS match. It ignores Node ID.
   **/
  public equals(otherRoute: RouteEntry) {
    return this.address === otherRoute.address && this.port === otherRoute.port;
  }
}

export function isRouteEntry(obj: unknown): obj is RouteEntry {
  return (
    typeof obj === 'object' &&
    obj != null &&
    'address' in obj &&
    'port' in obj &&
    'nodeId' in obj
  );
}

export default RouteEntry;
