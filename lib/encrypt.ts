/**
 * Classes used to hash/encrypt data
 **/

import { Cipher, createCipher, createDecipher, Decipher } from 'crypto';

/**
 * Base interface for Encode operations
 **/
export class BaseEncode {
  data: any;

  constructor(data: any) {
    this.data = data;
  }

  // Implement this to decode
  protected _decode(data: any) {
    throw 'decode method not implemented in this context';
  }

  // Implement this to encode
  protected _encode(data: any) {
    throw 'encode method not implemented in this context';
  }

  decode() {
    return this._decode(this.data);
  }

  encode() {
    return this._encode(this.data);
  }
}

/**
 * Class for Base64 encoding data
 * Inherits from BaseEncode
 **/
export class Base64Encode extends BaseEncode {
  constructor(data: any) {
    super(data);
  }

  protected override _encode(data: any) {
    return Buffer.from(data).toString('base64');
  }

  protected override _decode(data: any) {
    return Buffer.from(data, 'base64').toString('ascii');
  }
}

/**
 * Class for encrypting and decrypting data
 * Inherits from BaseEncode
 **/
export class Encrypt extends BaseEncode {
  algorithm: string;
  key: string;
  encrypted: string | null;
  cipher: Cipher;
  decipher: Decipher;

  constructor(data: any) {
    super(data);
    this.algorithm = 'aes256';
    this.key = 'sdflkjgdsfglkjdghdsglkdsfjghsglkdsfjhsdlgk';
    this.encrypted = null;
    this.cipher = createCipher(this.algorithm, this.key);
    this.decipher = createDecipher(this.algorithm, this.key);
  }

  protected override _encode(data: any) {
    if (!this.encrypted) {
      this.encrypted =
        this.cipher.update(this.data, 'utf8', 'hex') + this.cipher.final('hex');
    }
    return this.encrypted;
  }

  protected override _decode(data: any) {
    if (!this.encrypted) {
      return this.data;
    } else {
      return (
        this.decipher.update(this.encrypted, 'hex', 'utf8') +
        this.decipher.final('utf8')
      );
    }
  }
}
