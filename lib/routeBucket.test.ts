import RouteBucket from './routeBucket';
import RouteEntry from './routeEntry';

function makeExampleBucket() {
  return new RouteBucket([
    new RouteEntry('0.0.0.41', 8080, 'testId1'),
    new RouteEntry('0.0.0.42', 8080, 'testId2')
  ]);
}

describe('route_bucket', () => {
  it('initializes entries to empty array', () => {
    expect(new RouteBucket().entries).toStrictEqual([]);
  });

  it('stores route entries in bucket', () => {
    const bucket = makeExampleBucket();
    expect(bucket.entries.length).toBe(2);
    expect(bucket.entries[0].address).toBe('0.0.0.41');
    expect(bucket.entries[1].address).toBe('0.0.0.42');
  });

  it('route count is correct', () => {
    const bucket = makeExampleBucket();
    expect(bucket.getRouteCount()).toBe(2);
  });

  it('getAllRoutes() returns the routes', () => {
    const bucket = makeExampleBucket();
    expect(bucket.getAllRoutes().length).toBe(2);
    expect(bucket.getAllRoutes()[1].address).toBe('0.0.0.42');
  });

  // FIXME: it seems like addRoute and updateRoute are identical.
  it('addRoute() appends new routes', () => {
    const bucket = makeExampleBucket();
    expect(bucket.getRouteCount()).toBe(2);

    bucket.addRoute('a.b.c.d', 8080, 'id1');
    bucket.addRoute('a.b.c.d', 8080, 'id2');
    bucket.addRoute('a.b.c.d', 8081, 'id3');
    bucket.addRoute('x.x.x.x', 8082, 'id4');

    expect(bucket.getRouteCount()).toBe(5);
  });

  it('addRoute() updates existing nodeIDs', () => {
    const bucket = makeExampleBucket();

    bucket.addRoute('a.b.c.d', 8080, 'id5');
    expect(bucket.getAllRoutes()[2].nodeId).toBe('id5');

    bucket.addRoute('a.b.c.d', 8080, 'id6');
    expect(bucket.getAllRoutes()[2].nodeId).toBe('id6');
  });

  it('updateRoute() appends new routes', () => {
    const bucket = makeExampleBucket();
    expect(bucket.getRouteCount()).toBe(2);

    bucket.updateRoute('a.b.c.d', 8080, 'id1');
    bucket.updateRoute('a.b.c.d', 8080, 'id2');
    bucket.updateRoute('a.b.c.d', 8081, 'id3');
    bucket.updateRoute('x.x.x.x', 8082, 'id4');

    expect(bucket.getRouteCount()).toBe(5);
  });

  it('updateRoute() updates existing nodeIDs', () => {
    const bucket = makeExampleBucket();

    bucket.updateRoute('a.b.c.d', 8080, 'id5');
    expect(bucket.getAllRoutes()[2].nodeId).toBe('id5');

    bucket.updateRoute('a.b.c.d', 8080, 'id6');
    expect(bucket.getAllRoutes()[2].nodeId).toBe('id6');
  });
});
