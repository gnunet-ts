import Globals from "./globals";

/**
 * PingRequest objects hold data about PING operations waiting for a response
 **/
class PingRequest {
  address: string;
  port: number;
  resolved: boolean;
  timestamp: number;

  constructor(address: string, port: number) {
    this.address = address;
    this.port = port;
    this.resolved = false;
    this.timestamp = Date.now();
  }

  /**
   * Returns true if this PING request has expired
   **/
  isExpired() {
    return Date.now() > this.timestamp + Globals.Network.PING_MAX_TIME;
  }

  isResolved() {
    return this.resolved;
  }
  setResolved(resolved: boolean) {
    this.resolved = resolved;
  }
}

export default PingRequest;
