import Globals from './globals';

/**
 * Class used to find distances between keys
 **/
export class DistanceUtil {
  /**
   * Calculates the distance between 2 keys using the XOR metric
   * @param key1 {string}
   * @param key2 {string}
   * @return {int} The distance between two keys
   * @see http://en.wikipedia.org/wiki/Kademlia#Accelerated_lookups
   **/
  static calcDistance(key1: string, key2: string) {
    // Get binary strings
    const bin1 = DistanceUtil.hex2bin(key1);
    const bin2 = DistanceUtil.hex2bin(key2);
    let dis = 0;
    // For each bit, calculate distance
    for (let i = 0; i < bin1.length; i++) {
      const cb1 = parseInt(bin1[i]);
      const cb2 = parseInt(bin2[i]);
      dis += cb1 ^ cb2;
    }
    return dis;
  }

  /**
   * Converts hexadecimal code to binary code
   * @param hexString {string} A string containing single digit hexadecimal numbers
   **/
  static hex2bin(hexString: string) {
    let output = '';

    // For each hexadecimal character
    for (let i = 0; i < hexString.length; i++) {
      // Convert to decimal
      const decimal = parseInt(hexString.charAt(i), 16);

      // Convert to binary and add 0s onto the left as necessary to make up to 4 bits
      const binary = DistanceUtil.leftPadding(decimal.toString(2), '0', 4);

      // Append to string
      output += binary;
    }

    return output;
  }

  /**
   * Left pad a string with a certain character to a total number of characters
   * @param {string} inputString The string to be padded
   * @param {string} padCharacter The character that the string should be padded with
   * @param {number} totalCharacters The length of string that's required
   * @returns {string} A string with characters appended to the front of it
   **/
  static leftPadding(
    inputString: string,
    padCharacter: string,
    totalCharacters: number
  ) {
    // If the string is already the right length, just return it
    if (
      !inputString ||
      !padCharacter ||
      inputString.length >= totalCharacters
    ) {
      return inputString;
    }

    // Work out how many extra characters we need to add to the string
    const charsToAdd =
      (totalCharacters - inputString.length) / padCharacter.length;

    // Add padding onto the string
    for (let i = 0; i < charsToAdd; i++) {
      inputString = padCharacter + inputString;
    }

    return inputString;
  }
}

type HexDigit =
  | '0'
  | '1'
  | '2'
  | '3'
  | '4'
  | '5'
  | '6'
  | '7'
  | '8'
  | '9'
  | 'a'
  | 'b'
  | 'c'
  | 'd'
  | 'e'
  | 'f';

type GuidCharacter = HexDigit | '-';

/**
 * Guid Class used to generate random "GUID" like string. This CANNOT
 * be used in production to generate any kind of secure key
 **/
export class Guid {
  length: number;

  constructor(len: number) {
    this.length = len;
  }
  getGuid() {
    // http://www.ietf.org/rfc/rfc4122.txt
    const s: GuidCharacter[] = [];
    const hexDigits = '0123456789abcdef';
    for (let i = 0; i < this.length; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1) as HexDigit;
    }
    s[14] = '4'; // bits 12-15 of the time_hi_and_version field to 0010

    // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[19] = hexDigits.substr(((s[19] as any) & 0x3) | 0x8, 1) as HexDigit; // TODO: typing!

    s[8] = s[13] = s[18] = s[23] = '-';

    return s.join('');
  }
}

/**
 * Key class used for key parsing operations
 */
export class Key {
  key: string;

  constructor(key: string) {
    this.key = key;
  }

  /**
   * Parses a key into it's individual parts
   * @param {string} key
   * @returns {object}
   */
  static parse(key: string) {
    return Key._splitKey(key);
  }

  /**
   * Splits a key using the global delimiter and performs error checking
   * @param key
   * @returns {{namespace: *, key: *}}
   * @private
   */
  private static _splitKey(key: string) {
    const parts = key.split(Globals.KeyProps.KEY_DELIMITER);
    if (parts.length !== 2) {
      throw 'Unable to parse key. Malformed key.';
    }

    return {
      namespace: parts[0],
      key: parts[1]
    };
  }
}
