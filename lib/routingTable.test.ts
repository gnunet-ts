import RoutingTable from './routingTable';
import RPC from './rpc';

describe('RoutingTable', () => {
  it('should be initially empty', () => {
    expect(new RoutingTable({ port: 8080 }).getAllRoutes()).toStrictEqual([]);
  });

  it('should return the added routes', () => {
    const rt = new RoutingTable({ port: 8080 });
    rt.addRoute('127.0.0.1', 42, 'myId');
    rt.addRoute('127.0.0.1', 42, 'myId2');
    rt.addRoute('127.0.0.1', 43, 'myId3');
    expect(rt.getAllRoutes().map((e) => e.port)).toStrictEqual([42, 43]);
  });

  it('should remove a route', () => {
    const rt = new RoutingTable({ port: 8080 });
    rt.addRoute('127.0.0.1', 42, 'myId');
    rt.addRoute('127.0.0.1', 43, 'myId');
    expect(rt.getAllRoutes().length).toBe(2);
    rt.removeRoute('127.0.0.1', 43);
    expect(rt.getAllRoutes().length).toBe(1);
  });

  it('should return the correct number of random routes', () => {
    const rt = new RoutingTable({ port: 8080 });
    rt.addRoute('127.0.0.1', 42, 'myId');
    rt.addRoute('127.0.0.1', 43, 'myId2');
    rt.addRoute('127.0.0.1', 44, 'myId3');
    expect(rt.getRandomRoutes(1).length).toBe(1);
    expect(rt.getRandomRoutes(2).length).toBe(2);
    expect(rt.getRandomRoutes(3).length).toBe(3);
    expect(rt.getRandomRoutes(4).length).toBe(3);
  });

  it('should findByNodeId', () => {
    const rt = new RoutingTable({ port: 8080 });
    rt.addRoute('127.0.0.1', 42, 'myId');
    rt.addRoute('127.0.0.1', 43, 'myId2');
    expect(rt.findByNodeId('myId2')?.port).toBe(43);
  });

  it('should be able to query closest peers by key', () => {
    const rt = new RoutingTable({ port: 8080 });
    rt.addRoute('127.0.0.1', 42, 'myId');
    rt.addRoute('127.0.0.1', 43, 'myId2');
    expect(rt.getClosestPeers('byKey').length).toBe(2);
  });

  it('should be able to query closest peers by key', () => {
    const rt = new RoutingTable({ port: 8080 });
    rt.addRoute('127.0.0.1', 42, 'myId');
    rt.addRoute('127.0.0.1', 43, 'myId2');
    const rpc = new RPC();
    rpc.set('node_id', 7);
    expect(rt.getClosestPeers(rpc).length).toBe(1);
  });

  it('should return null if not found', () => {
    const rt = new RoutingTable({ port: 8080 });
    rt.addRoute('127.0.0.1', 42, 'myId');
    rt.addRoute('127.0.0.1', 43, 'myId2');
    expect(rt.findByNodeId('missing')).toBeNull();
  });
});
