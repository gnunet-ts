import Globals from './globals';

interface KeyValuePair {
  key: string;
  value: Value;
}

export interface KeyInnerValuePair {
  key: string;
  value: unknown;
}

export function isKeyValuePair(obj: unknown): obj is KeyInnerValuePair {
  return !!obj && typeof obj === 'object' && 'key' in obj && 'value' in obj;
}

/**
 * Class used to represent a value
 * Stores the actual data for the value along with associated meta data such as
 * create/update time.
 **/
export class Value {
  value: unknown;
  lastModifiedDate: number;

  constructor(value: unknown) {
    this.value = value;
    this.lastModifiedDate = Date.now();
  }

  /**
   * Checks if the value has expired based on the given time to live.
   *
   * @param {int} ttl
   * @return {boolean} - True if expired, false if not
   */
  expired(ttl: number) {
    return this.lastModifiedDate + ttl <= Date.now();
  }
}

interface MemCacheMap {
  [key: string]: Value | null;
}

/**
 * Basic memory cache storage. NOT PERSISTENT
 **/
class MemCache {
  map: MemCacheMap;

  constructor(map?: MemCacheMap) {
    this.map = map || {};
  }

  put(key: string, value: unknown) {
    this.map[key] = new Value(value);
  }

  get(key: string) {
    const val = this.map[key];
    if (!val) {
      return null;
    } else if (val.expired(Globals.ValueProps.TTL)) {
      this._delete(key);
      return null;
    }

    return val.value;
  }

  all(): KeyInnerValuePair[] {
    const list: KeyInnerValuePair[] = [];
    for (const key in this.map) {
      if (this.map.hasOwnProperty(key)) {
        const val = this.get(key);
        if (!val) {
          continue;
        }
        list.push({ key: key, value: val });
      }
    }
    return list;
  }

  /**
   * Returns all entries in the cache as Value objects
   * @returns {Array}
   */
  allAsValues(): KeyValuePair[] {
    const list: KeyValuePair[] = [];
    for (const key in this.map) {
      if (this.map.hasOwnProperty(key)) {
        const val = this.map[key];
        if (!val) {
          continue;
        }
        list.push({ key: key, value: val });
      }
    }
    return list;
  }

  containsKey(key: string): boolean {
    return key in this.map;
  }

  /**
   * Deletes the specified value for key
   * @param {string} key
   * @private
   */
  private _delete(key: string) {
    this.map[key] = null;
    delete this.map[key];
  }

  /**
   * Removes a value from the DB
   * @param key
   */
  removeValue(key: string) {
    this._delete(key);
  }
}

export default MemCache;
