import BaseKeyValidator from './keyValidator';

describe('key_validator', () => {
  it('should convert undefined key to an empty string', () => {
    expect(new BaseKeyValidator().key).toBe('');
  });

  it('should convert null key to an empty string', () => {
    expect(new BaseKeyValidator(null).key).toBe('');
  });

  it('should store a key', () => {
    expect(new BaseKeyValidator('myKey').key).toBe('myKey');
  });

  describe('isValid', () => {
    it('keys should be valid by default', () => {
      expect(new BaseKeyValidator().isValid()).toBe(true);
    });
  });

  describe('parse', () => {
    it('empty keys should be parsed', () => {
      expect(new BaseKeyValidator().parse()).toEqual({
        key: '',
        namespace: ''
      });
    });

    it('by default key and namespace are the same', () => {
      expect(new BaseKeyValidator('myKey').parse()).toEqual({
        key: 'myKey',
        namespace: 'myKey'
      });
    });
  });
});
