import { INITIAL_DELAY } from './asyncTask';
import DHT from './dht';
import { sleep } from './testHelper';

jest.setTimeout(INITIAL_DELAY + 2000);

describe('DHT', () => {
  it('should initialize correctly', () => {
    const dht = new DHT({ IP: '127.0.0.1', port: 1234 });
  });

  it('should start and stop', async () => {
    const dht = new DHT({ IP: '127.0.0.1', port: 1234 });
    expect(dht.start()).not.toBeNull();

    await sleep(1000);
    dht.stop();
    await sleep(INITIAL_DELAY + 500);
  });

  // TODO: more
});
