import RouteEntry from './routeEntry';

class RouteBucket {
  entries: RouteEntry[];

  constructor(entries?: RouteEntry[]) {
    // Array of RouteEntry's
    this.entries = entries || [];
  }

  addRoute(address: string, port: number, nodeId: string) {
    // Check if route exists
    // If it does, update node ID
    // If not add to entries
    const newRoute = new RouteEntry(address, port, nodeId);
    let existingRoute: RouteEntry | null = null;
    if (this.entries.length === 0) {
      this.entries.push(newRoute);
      existingRoute = newRoute;
    } else {
      this.entries.forEach(function (route) {
        if (route.equals(newRoute)) {
          existingRoute = route;
          return false;
        }
        return true;
      });
    }

    if (existingRoute == null) {
      this.entries.push(newRoute);
    } else {
      // Just update Node ID
      existingRoute.nodeId = newRoute.nodeId;
    }
  }

  getRouteCount() {
    return this.entries.length;
  }

  getAllRoutes() {
    return this.entries;
  }

  updateRoute(addr: string, port: number, nodeId: string) {
    if (this.entries.length === 0) {
      this.addRoute(addr, port, nodeId);
      return;
    }

    const newRoute = new RouteEntry(addr, port, nodeId);
    let updated = false;
    this.entries.forEach(function (route) {
      if (route.equals(newRoute)) {
        updated = true;
        route.nodeId = newRoute.nodeId;
        return false;
      }
      return true;
    });

    // Route not found, just create it
    if (!updated) {
      this.addRoute(addr, port, nodeId);
    }
  }
}

export default RouteBucket;
