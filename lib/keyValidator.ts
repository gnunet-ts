/**
 * The base key validator class
 * Provides an interface for all key validators
 **/
class BaseKeyValidator {
  key: string;

  constructor(key?: string | null) {
    this.key = key || '';
  }

  /**
   * Returns true if key is valid
   * @returns {bool}
   */
  public isValid() {
    return this._validateKey(this.key);
  }

  /**
   * Returns an array containing the split key string
   * @returns {[string]}
   */
  public parse() {
    return this._parse(this.key);
  }

  /**
   * Override this to validate a key. Return type needs to be boolean.
   * @param key
   * @return {boolean} - True if key is valid, false if not
   * @protected
   */
  protected _validateKey(key: string | null) {
    return true;
  }

  /**
   * Override this to split a key into it's various parts
   * @param key
   * @protected
   */
  protected _parse<T extends string | null>(key: T) {
    return { key: key, namespace: key };
  }
}

export default BaseKeyValidator;
