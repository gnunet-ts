/**
 * Socket file containing classes used for communication between nodes
 **/
import { createSocket, Socket } from 'dgram';
import RPC from './rpc';
import { make as makeLogger } from './logger';

const deferred = require('deferred');

const logger = makeLogger('R5N/sockets');

class UDPSocket {
  port: number;
  socket: Socket | null;

  constructor(port: number) {
    this.port = port;
    this.socket = null;
  }

  connect() {
    return this._init();
  }

  close() {
    return this.socket!.close();
  }

  private _init() {
    const self = this;
    const def = deferred();
    this.socket = createSocket('udp4');

    // Default error handler, can be overriden
    this.on('error', function (err: Error) {
      self.socket!.close();
      throw 'Socket error:\n' + err.stack;
    });

    this.on('listening', function () {
      logger.info('Socket listening on port %d', self.port);
    });

    this.socket.bind(this.port, function () {
      // Set max TTL
      // TTL in this context is the number of hops that packets
      // are allowed to travel. Each router decrements the TTL. Once
      // it hits 0, it is no longer forwarded.
      self.socket!.setTTL(128);

      def.resolve({ socket: self });
    });

    return def.promise();
  }

  on(evtType: string, cb: (...args: any[]) => void) {
    this.socket!.on(evtType, cb);
  }

  send(data: string, addr: string, port: number) {
    const def = deferred();
    // TODO: This should receive an RPC object
    const buf = Buffer.from(data);

    try {
      this.socket!.send(buf, 0, data.length, port, addr, function (err, bytes) {
        def.resolve({ error: err, bytes: bytes });
      });
    } catch (e) {
      def.reject(e instanceof Error ? e : new Error(`${e}`));
    }
    return def.promise();
  }

  onMessage(cb: any, ctx: any) {
    ctx = ctx || this;
    this.on('message', function (msg: any, rinfo: any) {
      const strData = Buffer.from(msg).toString();
      const rpc = new RPC(JSON.parse(strData));
      rpc.setRemoteData(rinfo);
      cb.call(ctx, rpc);
    });
  }
}

export default UDPSocket;
