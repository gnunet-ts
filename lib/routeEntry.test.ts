import RouteEntry from './routeEntry';

describe('route_entry', () => {
  it('assigns the correct fields', () => {
    const entry = new RouteEntry('1.2.3.4', 8080, 'myId');
    expect(entry.address).toBe('1.2.3.4');
    expect(entry.port).toBe(8080);
    expect(entry.nodeId).toBe('myId');
  });

  it('ignores node id for equality', () => {
    expect(
      new RouteEntry('a', 1, 'myId').equals(new RouteEntry('a', 1, 'otherId'))
    ).toBe(true);
  });

  it('requires matching address for equality', () => {
    expect(
      new RouteEntry('b', 1, 'myId').equals(new RouteEntry('a', 1, 'myId'))
    ).toBe(false);
  });

  it('requires matching port for equality', () => {
    expect(
      new RouteEntry('a', 1, 'myId').equals(new RouteEntry('a', 2, 'myId'))
    ).toBe(false);
  });
});
