import AsyncTask, { INITIAL_DELAY } from './asyncTask';
import { sleep } from './testHelper';

jest.setTimeout(INITIAL_DELAY + 1000);

describe('async task', () => {
  it('should set the name', () => {
    const fn = jest.fn();
    const task = new AsyncTask('my task', fn);
    expect(task.name).toBe('my task');
  });

  xit('should not start if stopped', async () => {
    const fn = jest.fn();
    const task = new AsyncTask('my task', fn);
    expect(task.stopped).toBe(false);
    task.stop();
    expect(task.stopped).toBe(true);
    task.start();
    await sleep(INITIAL_DELAY + 500);
    expect(task.stopped).toBe(true);
  });

  xit('should not call the function if stopped', async () => {
    const fn = jest.fn();

    const task = new AsyncTask('my task', fn);
    task.start();
    expect(fn).not.toBeCalled();
    await sleep(INITIAL_DELAY / 2);
    task.stop();
    await sleep(INITIAL_DELAY / 2 + 500);
    expect(fn).not.toBeCalled();
  });

  xit('should call the function', async () => {
    const fn = jest.fn();

    const task = new AsyncTask('my task', fn);
    task.start();
    expect(fn).not.toBeCalled();
    await sleep(INITIAL_DELAY + 500);
    expect(fn).toBeCalled();
    task.stop();
  });
});
