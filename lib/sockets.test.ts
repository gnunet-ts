import UDPSocket from './sockets';

describe('sockets', () => {
  it('initializes correctly', () => {
    const socket = new UDPSocket(8080);
    expect(socket.port).toBe(8080);
    expect(socket.socket).toBeNull();
  });

  it('throws when closing an un-connected socket', () => {
    const socket = new UDPSocket(8080);
    expect(() => socket.close()).toThrow(
      "Cannot read property 'close' of null"
    );
  });

  // TODO: more
});
