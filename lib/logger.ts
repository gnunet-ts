import * as winston from 'winston';

export const make = (name?: string) =>
  winston.createLogger({
    level: 'info',
    transports: [
      new winston.transports.Console({
        format: winston.format.combine(
          winston.format.colorize({ all: true }),
          winston.format.splat(),
          winston.format.simple()
        )
      })
    ]
  });
