import Globals from './globals';
import { DistanceUtil } from './utils';
import { make as makeLogger } from './logger';
import RoutingTable from './routingTable';
import RouteEntry from './routeEntry';
import DHT from './dht';

const deferred = require('deferred');

const logger = makeLogger('R5N/recursive_find');

interface WithIdAndRoutingTable {
  id: string;
  routingTable: RoutingTable;
}

/**
 * RecursiveFind clas uses to find values on DHT nodes
 * @param {DHT} dht The starting DHT
 * @param {string} key The key we are looking for
 **/
class RecursiveFind {
  startDHT: WithIdAndRoutingTable;
  key: string;

  constructor(dht: WithIdAndRoutingTable, key: string) {
    this.startDHT = dht;
    this.key = key;
  }

  start() {
    const def = deferred();
    // Get closest random node to the value
    // and recurse routing chains from there
    const firstRoute = this._getClosestRandomRoute();
    process.nextTick(
      RecursiveFind.prototype._find.bind(this, this.startDHT, def, 0)
    );
    return def.promise();
  }

  private _find(dht: DHT, def: any, r: number) {
    if (r > Globals.RecursiveRouting.MAX_STACK_SIZE) {
      // We have gone too far, return nothing
      def.reject(new Error('Nothing found for key: ' + this.key));
      return;
    }

    // First check this DHT, if none found search routing table
    // for closest node
    const data = dht.get(this.key);
    if (data) {
      def.resolve({ dht: dht, data: data });
      return;
    }

    const closeDHT = this._findClosest(dht, this.key);

    // TODO: This needs to send data to next node
    logger.debug('DHT: %j', closeDHT);
    this._find(closeDHT, def, ++r);
  }

  /**
   * Retrieves random nodes from the routing table to begin find
   **/
  private _getRandomRoutes() {
    return this.startDHT.routingTable.getRandomRoutes(
      Globals.RecursiveRouting.MAX_RANDOM_NODES
    );
  }

  /**
   * Retrieves random nodes from the routing table to begin find
   **/
  private _getClosestRandomRoute() {
    const self = this;
    const randomRoutes = this._getRandomRoutes();
    let closest: RouteEntry | null = null;
    let closeDis = -1;
    randomRoutes.forEach(function (route) {
      const dis = DistanceUtil.calcDistance(self.startDHT.id, route.nodeId);
      if (closeDis === -1) {
        closest = route;
      } else if (dis < closeDis) {
        closeDis = dis;
        closest = route;
      }
    });
    return closest;
  }

  /**
   * Finds closest match in DHTs routing table
   **/
  private _findClosest(dht: DHT, key: string): RouteEntry | null {
    const routes = dht.routingTable.getAllRoutes();
    let closest: RouteEntry | null = null;
    let closeDis = -1;
    routes.forEach(function (route) {
      const dist = DistanceUtil.calcDistance(route.nodeId, key);
      if (closeDis === -1) {
        closest = route;
      } else if (dist < closeDis) {
        closeDis = dist;
        closest = route;
      }
    });
    return closest;
  }
}

export default RecursiveFind;
