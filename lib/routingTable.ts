/**
 * Routing table for DHT.
 **/
import Globals from './globals';
import RouteEntry from './routeEntry';
import RouteBucket from './routeBucket';
import { DistanceUtil } from './utils';
import { make as makeLogger } from './logger';
import RPC from './rpc';

const logger = makeLogger('R5N/routing_table');

interface WithPort {
  port: number;
}

class RoutingTable {
  dht: WithPort;
  bucketList: RouteBucket[];

  constructor(dht: WithPort) {
    this.dht = dht;
    // List of RouteBucket's
    this.bucketList = [];
    this.bucketList.push(new RouteBucket());
  }

  addRoute(address: string, port: number, nodeId: string) {
    const self = this;
    // TODO: Need to check if table is full
    // If so, need to ping nodes and remove stale entries
    // if (this.routeList.length >= Globals.Routing.ROUTE_TABLE_MAX_SIZE) {
    //    throw 'TODO: Implement full routing table logic'
    // }

    // TODO: Bucket size in globals
    let added = false;
    logger.silly('Add route: %s, %d', address, port);
    self.bucketList.forEach(function (bucket) {
      if (bucket.getRouteCount() >= 10) {
        return true;
      } else {
        if (!self._routeExists(address, port)) {
          bucket.addRoute(address, port, nodeId);
        }

        added = true;
        return false;
      }
    });

    if (!added) {
      self.bucketList.push(new RouteBucket());
      const lastBucket = self.bucketList[self.bucketList.length - 1];

      if (self._routeExists(address, port)) {
        //lastBucket.updateRoute(address, port, nodeId);
      } else {
        lastBucket.addRoute(address, port, nodeId);
      }
    }
  }

  /**
   * Returns all routes for this table (all buckets)
   **/
  getAllRoutes() {
    const routeList: RouteEntry[] = [];
    for (let i = 0; i < this.bucketList.length; i++) {
      this.bucketList[i].getAllRoutes().forEach(function (e) {
        routeList.push(e);
      });
    }
    return routeList;
  }

  /**
   * Returns true if route already exists in the given route list
   **/
  private _routeExists(addr: string, port: number) {
    let exists = false;
    this.getAllRoutes().forEach(function (currentRoute) {
      if (currentRoute.address === addr && currentRoute.port === port) {
        exists = true;
        return false;
      }
      return true;
    });
    return exists;
  }

  /**
   * Returns a random set of routes from the routing table
   * @param {int} max number of routes to return
   * @return {[RouteEntry]}
   **/
  getRandomRoutes(max: number) {
    // Check to see if we have less than max number of routes
    // If so, just return them
    const routeList: RouteEntry[] = this.getAllRoutes();
    if (routeList.length <= max) {
      logger.debug('Tiny route list in %d: %j', this.dht.port, routeList);
      return routeList;
    }

    const randList: RouteEntry[] = [];
    const randNums: number[] = [];
    // Try {max} times to fill array with random routes
    for (let i = 0; i < max; i++) {
      const randInt = Math.floor(Math.random() * routeList.length);
      if (randNums.indexOf(randInt) > -1) {
        // Need to decrement counter so we continue to loop
        i--;
        continue;
      }

      randNums.push(randInt);
      randList.push(routeList[randInt]);
    }
    logger.silly('RANDOM ROUTES: %j', randList);
    return randList;
  }

  /**
   * Removes the given IP/Port from the routing table
   **/
  removeRoute(addr: string, port: number) {
    this.bucketList.forEach(function (bucket) {
      for (let i = 0; i < bucket.entries.length; i++) {
        const entry = bucket.entries[i];
        if (entry.address === addr && entry.port === port) {
          bucket.entries.splice(i--, 1);
          return false;
        }
      }
      return true;
    });
  }

  /**
   * Retrieves k closest nodes to the entries in the routing table
   * @param {RPC/string} rpc Can be a node ID or key for a value in hash table
   * @return {[RouteEntry]}
   **/
  getClosestPeers(rpc: RPC | string) {
    if (typeof rpc === 'string') {
      return this._getClosestPeersByKey(rpc);
    }

    const routes = this.getAllRoutes();

    if (routes.length === 0) {
      return routes;
    }
    // Array of distances
    const distanceList: number[] = [];
    // Map of distance -> route
    const distMap: { [key: number]: RouteEntry } = {};
    // Information from sending node
    const key = rpc.get('node_id') as string; // TODO: typing!
    const fromAddr = rpc.fromAddress;
    const fromPort = rpc.fromPort;

    routes.forEach(function (route) {
      // If this route is the same as the sending node's route then skip it
      if (
        route.nodeId === key ||
        (route.address === fromAddr && route.port === fromPort)
      ) {
        return true;
      }

      const dist = DistanceUtil.calcDistance(key, route.nodeId);
      if (distanceList.indexOf(dist) > -1) {
        return true;
      }
      distanceList.push(dist);
      distMap[dist] = route;
      return true;
    });

    // Sort array
    const compareNumbers = function (a: number, b: number) {
      return a - b;
    };
    distanceList.sort(compareNumbers);

    const returnRoutes: RouteEntry[] = [];
    for (let n = 0; n < Globals.Const.K && n < distanceList.length; n++) {
      // Add routes from distance map according to sorted array
      returnRoutes.push(distMap[distanceList[n]]);
    }

    return returnRoutes;
  }

  _getClosestPeersByKey(key: string) {
    const routes = this.getAllRoutes();

    if (routes.length === 0) {
      return routes;
    }
    // Array of distances
    const distanceList: number[] = [];
    // Map of ditance -> route
    const distMap: { [dist: number]: RouteEntry } = {};

    routes.forEach(function (route) {
      if (route.nodeId === key) {
        return true;
      }
      const dist = DistanceUtil.calcDistance(key, route.nodeId);
      if (distanceList.indexOf(dist) > -1) {
        return true;
      }
      distanceList.push(dist);
      distMap[dist] = route;
      return true;
    });

    // Sort array
    const compareNumbers = function (a: number, b: number) {
      return a - b;
    };

    distanceList.sort(compareNumbers);

    const returnRoutes: RouteEntry[] = [];
    for (let n = 0; n < Globals.Const.K && n < distanceList.length; n++) {
      // Add routes from distance map according to sorted array
      returnRoutes.push(distMap[distanceList[n]]);
    }

    return returnRoutes;
  }

  /**
   * Looks up a routing entry based on the given node ID
   **/
  findByNodeId(nodeId: string): RouteEntry | null {
    let retRoute: RouteEntry | null = null;
    this.getAllRoutes().forEach(function (route) {
      if (route.nodeId === nodeId) {
        retRoute = route;
        return false;
      }
      return true;
    });
    return retRoute; // TODO: ts incorrectly infers this as null.
  }
}

export default RoutingTable;
