import MemCache, { Value } from './memCache';

describe('mem_cache', () => {
  it('constructor falls back to empty array', () => {
    expect(new MemCache().map).toStrictEqual({});
  });
  it('constructor copies map', () => {
    const map = new MemCache({ a: new Value('b'), c: new Value('d') }).map;
    expect(Object.keys(map).length).toBe(2);
    expect(map.a?.value).toBe('b');
    expect(map.c?.value).toBe('d');
  });

  it('reads and writes values', () => {
    const cache = new MemCache();
    cache.put('a', 'foo');
    cache.put('b', 'foo');
    cache.put('a', 'foo2');
    expect(cache.get('a')).toStrictEqual('foo2');
    expect(cache.get('b')).toStrictEqual('foo');
  });

  it('all returns truthy values', () => {
    const cache = new MemCache();
    cache.put('a', 0);
    cache.put('b', 'foo');
    cache.put('c', undefined);
    cache.put('d', 'bar');
    expect(cache.all()).toStrictEqual([
      { key: 'b', value: 'foo' },
      { key: 'd', value: 'bar' }
    ]);
  });

  it('allAsValues returns all values as Value Objects', () => {
    const cache = new MemCache();
    cache.put('a', 0);
    cache.put('b', 'foo');
    cache.put('c', undefined);
    cache.put('d', 'bar');

    const allValues = cache.allAsValues();
    expect(allValues.length).toBe(4);
    expect(allValues[0].key).toBe('a');
    expect(allValues[0].value.value).toBe(0);
    expect(allValues[1].key).toBe('b');
    expect(allValues[1].value.value).toBe('foo');
    expect(allValues[2].key).toBe('c');
    expect(allValues[2].value.value).toBe(undefined);
    expect(allValues[3].key).toBe('d');
    expect(allValues[3].value.value).toBe('bar');
  });

  it('containsKey works', () => {
    const cache = new MemCache();

    cache.put('a', 0);
    expect(cache.containsKey('a')).toBe(true);
    expect(cache.containsKey('b')).toBe(false);
  });

  it('removeValue removes a value by key', () => {
    const cache = new MemCache();

    cache.put('a', 42);
    cache.put('b', 43);
    cache.removeValue('a');

    expect(cache.containsKey('a')).toBe(false);
    expect(cache.containsKey('b')).toBe(true);
  });
});
