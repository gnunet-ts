import { DistanceUtil, Guid, Key } from './utils';

describe('utils', () => {
  describe('DistanceUtil', () => {
    describe('calcDistance', () => {
      it('should work for empty strings', () => {
        expect(DistanceUtil.calcDistance('', '')).toBe(0);
        expect(DistanceUtil.calcDistance('a', '')).toBe(2);
        expect(DistanceUtil.calcDistance('', 'b')).toBe(0);
        expect(DistanceUtil.calcDistance('b', 'a')).toBe(1);
      });

      it('should compute the correct distance', () => {
        expect(DistanceUtil.calcDistance('testing123', 'test')).toBe(4);
      });

      it('should have zero distance from self', () => {
        expect(DistanceUtil.calcDistance('Foo', 'Foo')).toBe(0);
      });

      it('should not necessarily be symmetric', () => {
        expect(DistanceUtil.calcDistance('blahBlub', 'blFoo')).toBe(8);
        expect(DistanceUtil.calcDistance('blFoo', 'blahBlub')).toBe(5);
      });
    });

    describe('hex2bin', () => {
      it('should handle empty strings', () => {
        expect(DistanceUtil.hex2bin('')).toBe('');
      });

      it('should print NaN if a non-hex digit is entered', () => {
        expect(DistanceUtil.hex2bin('foo')).toBe('11110NaN0NaN');
      });

      it('should covert correctly', () => {
        expect(DistanceUtil.hex2bin('0')).toBe('0000');
        expect(DistanceUtil.hex2bin('00')).toBe('00000000');
        expect(DistanceUtil.hex2bin('10')).toBe('00010000');
        expect(DistanceUtil.hex2bin('a0')).toBe('10100000');
        expect(DistanceUtil.hex2bin('efaec0')).toBe('111011111010111011000000');
      });
    });

    describe('leftPadding', () => {
      it('should work with 0 length', () => {
        expect(DistanceUtil.leftPadding('', 'x', 0)).toBe('');
        expect(DistanceUtil.leftPadding('abc', 'x', 0)).toBe('abc');
      });

      // FIXME
      it('BUG: should return the same result if empty', () => {
        expect(DistanceUtil.leftPadding('', 'x', 5)).toBe('');
      });

      it('may expand padding has more than one character', () => {
        expect(DistanceUtil.leftPadding('a', 'xy', 6)).toBe('xyxyxya');
      });

      it('should pad correctly', () => {
        expect(DistanceUtil.leftPadding('a', 'x', 5)).toBe('xxxxa');
      });
    });
  });

  describe('Guid', () => {
    it('should have the specified length', () => {
      expect(new Guid(0).getGuid().length).toBe(6);
      expect(new Guid(4).getGuid().length).toBe(10);
      expect(new Guid(12).getGuid().length).toBe(17);
      expect(new Guid(23).getGuid().length).toBe(24);
      expect(new Guid(24).getGuid().length).toBe(24);
      expect(new Guid(30).getGuid().length).toBe(30);
    });

    it('should change between calls', () => {
      expect(new Guid(10).getGuid()).not.toBe(new Guid(10).getGuid());
    });
  });

  describe('Key', () => {
    it('should set key as property', () => {
      expect(new Key('abc').key).toBe('abc');
    });

    it('should throw if format is invalid correctly', () => {
      expect(() => Key.parse('abc')).toThrow(
        'Unable to parse key. Malformed key.'
      );
    });

    it('should throw if format is invalid correctly', () => {
      expect(() => Key.parse('abc:123:xxx')).toThrow(
        'Unable to parse key. Malformed key.'
      );
    });

    it('should parse correctly', () => {
      const parsed = Key.parse('abc:123');
      expect(parsed.namespace).toBe('abc');
      expect(parsed.key).toBe('123');
    });
  });
});
