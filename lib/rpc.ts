/**
 * RPC (remote procedure call) class used for all message transfers
 **/
import { Guid } from './utils';

type DataObject = {
  id?: string | null;
  type?: string | null;
  address?: string | null;
  port?: number | null;

  [key: string]: unknown;
};

class RPC {
  data: DataObject;
  id: string;

  type: string | null;
  address: string | null;
  port: number | null;

  fromAddress: string | null;
  fromPort: number | null;
  fromFamily: string | null;
  packetSize: number | null;

  constructor(data?: DataObject) {
    this.data = data || {};
    this.id = this.data.id || new Guid(64).getGuid();
    // Destination info
    this.type = this.data.type || null;
    this.address = this.data.address || null;
    this.port = this.data.port || null;
    // Remote info
    this.fromAddress = null;
    this.fromPort = null;
    this.fromFamily = null;
    this.packetSize = null;
  }

  setRemoteData(rinfo: {
    address: string;
    port: number;
    family: string;
    size: number;

    [key: string]: unknown;
  }) {
    this.fromAddress = rinfo.address;
    this.fromPort = rinfo.port;
    this.fromFamily = rinfo.family;
    this.packetSize = rinfo.size;
  }
  serialize() {
    // Add extra fields onto data
    this.data.id = this.id;
    this.data.type = this.type;
    this.data.address = this.address;
    this.data.port = this.port;
    return JSON.stringify(this.data);
  }

  set(key: string, value: unknown) {
    this.data[key] = value;
  }

  get(key: string) {
    return this.data[key];
  }

  getStringOrThrow(key: string, msg = 'expected a string') {
    const str = this.get(key);
    if (typeof str !== 'string') {
      throw new Error(msg);
    }
    return str;
  }

  getNumberOrThrow(key: string, msg = 'expected a number') {
    const str = this.get(key);
    if (typeof str !== 'number') {
      throw new Error(msg);
    }
    return str;
  }
}

export default RPC;
