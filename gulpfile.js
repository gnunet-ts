'use strict';

var fs = require('fs');
var gulp = require('gulp');
var jshint = require('gulp-jshint');
var jscs = require('gulp-jscs');
var semver = require('semver');
var execSync = require('execSync');

var packageFilename = './package.json';

gulp.task('lint', function() {
    return gulp.src('./lib/**/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jscs('./.jscs.json'));
});

/**
 * Internal tasks that define the 'publish' task.
 * Call `gulp publish` or
 * `gulp publish-minor` or `gulp publish-major` or `gulp publish-patch`.
 */
function publishTask(type) {
    if (typeof type === 'undefined') {
        type = 'patch';
    }
    execSync.run('git checkout develop');
    var pkg = require(packageFilename);
    pkg.version = semver.inc(pkg.version, type);
    fs.writeFileSync(packageFilename, JSON.stringify(pkg, null, 2));
    execSync.run('git commit -a -m "Bumped version"');
    execSync.run('git push origin develop');
    execSync.run('git checkout master');
    execSync.run('git merge --no-ff develop -m "Merged develop into master"');
    execSync.run('git push origin master');
    execSync.run('git tag v' + pkg.version);
    execSync.run('git push origin --tags');
    execSync.run('npm publish');
    execSync.run('git checkout develop');
}
gulp.task('publish-patch', function() {
    publishTask('patch');
});
gulp.task('publish-minor', function() {
    publishTask('minor');
});
gulp.task('publish-major', function() {
    publishTask('major');
});
gulp.task('publish', ['publish-patch']);

gulp.task('default', ['lint']);
