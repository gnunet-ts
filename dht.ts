import DHT from './lib/dht';

import { make as makeLogger } from './lib/logger';

const logger = makeLogger('R5N/dht');

// Put the IP address and port for the DHT here
// NOTE: This is the public IP for this node, not localhost or 127.0.0.1
const address = '127.0.0.1';
const port = 8001;

// Instantiate new DHT and start it
const dht = new DHT({
  IP: address,
  port: port
});

dht
  .start()
  .then(
    function (_res: any) {
      logger.info('DHT started');
      dht
        .bootstrap('127.0.0.1', 8000)
        .then(function (res: any) {
          logger.info(
            'Bootstrap complete with %d routes',
            res.number_of_routes
          );
          dht
            .store('BLAH', 'VALUE')
            .then(function (res: any) {
              logger.info('RESULT: %j', {
                key: res.key,
                value: res.value,
                nodeId: res.rpc.get('stored_at')
              });
              logger.info(
                'Store complete!\nKey %s was stored at node ID: %s',
                res.rpc.get('key'),
                res.rpc.get('stored_at')
              );
            })
            .catch(function (err: any) {
              logger.error('Error in STORE RPC: %s', err);
            });
        })
        .catch(function (err: any) {
          logger.error('ERROR: %s', err);
        });
    },
    function (err: any) {
      logger.error('ERROR: %s', err);
    }
  )
  .catch(function (err: any) {
    logger.error('ERROR: %s', err);
  });
